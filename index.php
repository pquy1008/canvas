<!DOCTYPE html>
<html>
<head>
	<title>Canvas</title>
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.12.0.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.4.5/socket.io.min.js"></script>
	<link rel="stylesheet" type="text/css" href="link/style.css">
</head>

<body>
	<h1>Canvas Caro</h1>
	<div class="container">
		<div class="playArea">
			<canvas id="canvasArea" width="505" height="505">
				Canvas is not support in your browser, Please enable Javascript or upgrate to newer browser's version.
			</canvas>
		</div>

		<div class="terminal">
			<div class="message">Xin chào đến với Caro game online!!!</div>
		</div>
	</div>

	<script src="link/main.js"></script>
</body>
</html>