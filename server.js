var express = require("express");
var app = express();
var port = 8081;
const FREE = 0;
const PLAYING = 1;

app.get("/", function(req, res) {
	res.send("It works");
});

var io = require('socket.io').listen(app.listen(port));
var allSockets = [];
io.sockets.on('connection', function (socket) {
	//Update allSocket
	allSockets.push(socket);

	//Update my status to FREE
	socket.status = FREE;

	console.log('user change: ' + allSockets.length);

	//Notice connect successful
	socket.emit('init', 'Socket is connected');

	//Find competitor and pairing
	pairing();

	socket.on('hit', function(data) {
		//Emit to competitor
        socket.competitor.emit('hit', {cx: data.cx, cy: data.cy});
	})

	socket.on('findCompetitor', function(data) {
		console.log('find Competitor');

		//Update status to FREE
		socket.status = FREE;

		//Find competitor
		pairing();
	})

	socket.on('disconnect', function() {
		//Notice to competitor
		socket.competitor.emit('competitorDisconnect', true);

		//Remove socket from allSocket
		var index = allSockets.indexOf(socket);
		allSockets.splice(index, 1);

		console.log('user change: ' + allSockets.length);
	})

	function pairing() {
		var competitor = null;
		for(var k in allSockets) {
			//I'm not my competitor
			if(allSockets[k] == socket) {
				continue;
			}

			if(allSockets[k].status == FREE) {
				competitor = allSockets[k];
				break;
			}
		}

		if(competitor !== null) {
			//Set competitor to me on allSocketDatas
			socket.competitor = competitor;

			//Set me to competitor on allSocketDatas
			competitor.competitor = socket;

			//Notice to me
			socket.emit('pair', {
				status: 1,
				competitorID: competitor.id
			});

			//Set my status to PLAYING
			socket.status = PLAYING;

			//Notice to competitor
			competitor.emit('pair', {
				status: 1,
				competitorID: socket.id
			})

			//Set competitor's status to PLAYING
			competitor.status = PLAYING;
		}
		else {
			//Notice to me, have not competitor available
			socket.emit('pair', {
				'status': 0
			});
		}
	}
});
console.log("Listening on port " + port);

