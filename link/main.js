
var socket = io.connect('http://192.168.33.30:8081/');

var competitorID = "";
var dataMatrix;
var canvas;		//canvas element
var ctx;		//Canvas object

var oTicket = new Image();
var xTicket = new Image();

if(init() == true) {
	socket.on('init', function(data) {
		console.log("Socket init: " + data);
		console.log('find competitor...');

		logMessage("Kết nối máy chủ thành công");
		logMessage("Tìm kiếm đối thủ ...");
	})

	socket.on('pair', function(data) {
		if(data.status == 0) {
			console.log("Have not competitor, waiting competitor....");
			logMessage("Hiện tại không có đối thủ nào sẵn sàng, hãy đợi ai đó vào cuộc ...");

			//Remove listen even 'click'
			canvas.removeEventListener('click', hit);
		} else {
			competitorID = data.competitorID;
			console.log("Ok, competitorID: " + competitorID);

			logMessage("Đã tìm đc đối thủ, ID: " + competitorID);

			//Add listen even 'click'
			clearCanvas();
			canvas.addEventListener('click', hit);
		}
	})

	socket.on('hit', function(data) {
		console.log("Competitor hit: ");
		console.log(data);

		competitorDrawnSign(data);
	})

	socket.on('competitorDisconnect', function(data) {
		logMessage("Đối thủ của bạn quá sợ hãi nên đã thoát. :))");
		logMessage("-----------------------------------------------------");

		logMessage("Tìm kiếm đối thủ mới ...");

		//Find the other competitor
		findOtherCompetitor();

		//Remove listen 'click'
		canvas.removeEventListener('click', hit);
	})

	socket.on('disconnect', function() {
		logMessage("Mất kết nối từ server. Xin chia buồn");
		canvas.removeEventListener("click", hit)
	})
}

function logMessage(msg) {
	$(".terminal").append("<div class='message'>" + msg + "</div>");
}

function findOtherCompetitor() {
	socket.emit('findCompetitor', true);
}

function clearCanvas() {
	dataMatrix = {};
	for(var i = 0; i <= 20; i++) {
		dataMatrix[i] = {};
		for(var j = 0; j <= 20; j++) {
			dataMatrix[i][j] = {
				mark: false,
				who: null
			}
		}
	};
	ctx.clearRect(0, 0, canvas.width, canvas.height);
}

var hit = function(e) {
	var cx;
	var cy;

	cx = Math.floor(e.layerX/24);
	cy = Math.floor(e.layerY/24);

	drawnSign({cx: cx, cy: cy});
}

//Draw sign
var drawnSign = function(position) {
	var cx = position.cx;
	var cy = position.cy

	if(dataMatrix[cx][cy].mark === false) {
		//Update dataMatrix
		dataMatrix[cx][cy].mark = 0;
		dataMatrix[cx][cy].who = "me";

		//Socket emit hit
		console.log({cx: cx, cy: cy});
		socket.emit('hit', {
			competitorID: competitorID,
			cx: cx,
			cy: cy
		})

		//Update canvas
		ctx.drawImage(oTicket, cx * 24, cy * 24);
		//ctx.fillRect(cx * 24, cy * 24, 24, 24);
	} else {
		//alert("Is Pressed before");
	}

	if(checkVictory(position) == true) {
		logMessage("Bạn đã thắng, chúc mừng :v :v :v :v");
	};
}

var competitorDrawnSign = function (position) {
	var cx = position.cx;
	var cy = position.cy

	if(dataMatrix[cx][cy].mark === false) {
		//Update dataMatrix
		dataMatrix[cx][cy].mark = 1;
		dataMatrix[cx][cy].who = "competitor";

		//Update canvas
		ctx.drawImage(xTicket, cx * 24, cy * 24);
		//ctx.fillRect(cx * 24, cy * 24, 24, 24);
	} else {
		//alert("Is Pressed before");
	}
}

function readyToPlay(ready) {
	if(ready == true) {
		//ready
	}
	else {
		//not ready
	}
}

function init() {
	//Init oticket, xticket
	oTicket.src = 'link/o.png';
	xTicket.src = 'link/x.png';

	//Drawn canvas
	canvas = document.getElementById('canvasArea');
	if (!canvas.getContext){
		console.log("Canvas is not support");

		logMessage("Trình duyệt của bạn không hỗ trợ game, hãy dùng Chrome");
		return false;
	}
	ctx = canvas.getContext('2d');
	return true;
}

function checkVictory(currentPosition) {
	var cx = currentPosition.cx;
	var cy = currentPosition.cy;

	var win = false;
	var count = 0;

	var min = 0;
	var max = 20;

	//Check hang ngang
	count = 0;
	min = cx - 4;
	min = (min < 0? 0: min);

	max = cx + 4;
	max = (max > 20? 20: max);

	for( var i = min; i <= max; i++ ) {
		if (dataMatrix[i][cy].mark === 0) {
			count++;

			if(count == 5) {
				win = true;
				console.log("I'm victory");

				logMessage("Bạn đã thắng, chúc mừng :v :v :v :v");
				return;
			}
		}
		else {
			count = 0;
			continue;
		}
	}


	//Check hang doc
	count = 0;
	min = cy - 4;
	min = (min < 0? 0: min);

	max = cy + 4;
	max = (max > 20? 20: max);

	for( var i = min; i <= max; i++ ) {
		if (dataMatrix[cx][i].mark === 0) {
			count++;

			if(count == 5) {
				win = true;
				return win;
			}
		}
		else {
			count = 0;
			continue;
		}
	}

	//Check hang cheo, tu tren xuong
	count = 0;
	var minx = cx - 4;
	var miny = cy - 4;

	for(var i = 0; i <= 8; i++) {
		if(dataMatrix[minx + i] == null || dataMatrix[minx + i][miny + i] == null || dataMatrix[minx + i][miny + i].mark !== 0) {
			count = 0;
			continue;
		} else {
			count++;

			if(count == 5) {
				win = true;
				return win;
			}
		}
	}

	//Check hang cheo, tu duoi len
	count = 0;
	var minx = cx - 4;
	var miny = cy + 4;

	for(var i = 0; i <= 8; i++) {
		if(dataMatrix[minx + i] == null || dataMatrix[minx + i][miny - i] == null || dataMatrix[minx + i][miny - i].mark !== 0) {
			count = 0;
			continue;
		} else {
			count++;

			if(count == 5) {
				win = true;
				return win;
			}
		}
	}

	return win;
}





